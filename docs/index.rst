.. sohbet documentation master file, created by
   sphinx-quickstart on Sun Mar 28 11:35:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sohbet Odaları - Bedava Web Chat Sohbet Mobil Sohbet Siteleri
==================================
https://www.sohbete.com.tr Sohbet odaları, kullanıcıların mobil, bilgisayar ve internet bağlantılı cihazları ile web sohbet, mobil chat, mirc yazılım ve mobil uygulamaları kullanarak, sohbet edebilecekleri, arkadaş bulabilecekleri, eğlenceli ve keyifli zaman geçirebilecekleri irc network ağı içerisinde oluşturulan odalardır. 

Sohbet Odaları, kullanıcıların çok sayıda kullanıcıyla sohbet chat yapabilmesi, alternatif seçenekler sağlaması için birden fazla sohbet odası veya chat sitesinin irc network ağı üzerinde oluşturulan odalara sohbet, mirc, mobil giriş seçeneklerini birleştirerek veya giriş seçeneklerini yönlendirme yöntemiylede oluşturulabilir. Sohbet girişi veren yada yönledirilen siteler adına irc network ağı üzerinde oda açılarak birden fazla oda/kanalda sohbet edilmesi kullanıcıların alternatif sohbet edecek kullanıcılar ile sohbet ederek tanışmasına, daha fazla arkadaş ve sohbet edecek konu bulmasına eğlenceli ve keyifli zaman geçirmelerini sağlamaktadır. 

Kullanıcı sayısı ve sohbet edilecek oda sayısının fazla ve alternatifinin olması kullanıcıların sıkılmadan sohbet ederek sohbet odalarında daha fazla zaman geçirmelerine, arkadaş ve dostlarının sayısının artmasıyla daha geniş bir sosyal çevreye sahip olmasını sağlar. IRC network ağı üzerinde oluşturulan sohbet odaları haricinde kullanıcıların müzik dinleyebilmeleri için irc sunucularının olmazsa olmazı sayılan Radyo odası bulunmaktadır. 

Radyo yayını dinleme odası kullanıcıların deneyimli radyo djlerinin yayın akışlarına katılarak dinlemek istedikleri yada arkadaşlarına armağan etmek istedikleri parçaları istek yaparak dinyebildikleri, sohbet ederek müzik dolu zaman geçirebildikleri odadır. Sohbet odaları içerisinde kullanıcıların oyun oynama, yarışma istekleride göz önünde bulundurularak, eğlenme, keyifli zaman geçirme, yarışırken sohbet edebildikleri oyun odaları bulunmaktadır. Oyun odaları belli başlı su kanallardan oluşmaktadır: #Oyun, #Çarkıfelek, #Yarişma, #Oxm, #Kelime, #Şişeçevir, #İslam. Kullanıcılar beğendikleri ve sevdikleri oyun robotları ile bilgi ve kelime haznelerini genişletirkeni yarışmalar sırasında birbirleri ile sohbet ederek arkadaşlık kurabilir, samimi ve sıcak dostlara sahip olabilir. 

Sohbet Odaları içerisinde odanın düzeni, kullanıcıların rahat ve ferah bir ortamda sohbet edebilmeleri için çeşitli kurallar ve bu kuralları uygulayan yetkililer: %Halfop, @Aop, &Sop, ~Founder simge ve isimleri ile görev yaparlar. Kullanıcıların kanal içerisinde rahatsız edilmeden seviyeli ve kaliteli sohbet edebilmeleri için yetkili oldukları odaları izler sohbete katılarak konu ve gündemi belirler, odanın düzenini ve sohbetin akışını bozan, rahatsızlık veren kullanıcı yada kullanıcıları odadan uzaklaştırırlar. IRC network ağp içerisindeki odalarda kullanılan servisleride kısaca belirtelim: NickServ: Kullanıcıların nickname (Rumuz/Kullanıcı adı) şifreyelerek kayıt etmelerinde kullanılan irc servisidir. Bu servisi kullanarak diğer kullanıcıların rumuzunuzu kullanmasını engelleyebilirsiniz. ChanServ: Sohbet ve diğer kullanıcıların kullandığı odaların şifrelenerek kayıt edilmesini sağlayan irc servisidir. MemoServ: Kullanıcıların kayıtlı kullanıcılara özel mesaj bırakabildikleri irc servisidir. Kullanıcılar mobil uyumlu tüm servis ve arayüzlerini üye olmadan kullanarak hemen sohbet edebilirler.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Yararlı Bağlantılar:
==================
* https://www.sohbetet.net.tr
* https://www.goruntuluchat.net.tr
* https://www.websohbet.org.tr
* https://www.omegletv.org.tr
* https://websohbet.xyz
